from alpha_vantage.foreignexchange import ForeignExchange 
import os

##Exchange##
class Exchange_connexion():
    """
     ForeignExchange connexion


    Returns:
        [type]: Singleton connexion
    """
    
    __instance=None

    def __new__(cls) :
        if cls.__instance is None:
         
            cls.__instance = object.__new__(cls)
            Key=os.environ.get("VANTAGE_KEY")
            format=os.environ.get("OUTPUT_FORMAT")
            cls.__instance.Exchange=ForeignExchange(key="MVDGLT2IXJSFSSSN",output_format="pandas")
        return cls.__instance
       

  