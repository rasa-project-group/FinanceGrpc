from ServerGrpc.entities.Requests import Request_Currecny_Image_interval, Requst_Currecny_Image
from typing import List
from pandas import DataFrame
from .Connexion.Foreigen_currency import Exchange_connexion
from alpha_vantage.foreignexchange import ForeignExchange
from alpha_vantage.alphavantage import AlphaVantage
from protos_output.finance_pb2 import CurrencyConv_Response,CurrencyExchange_Image_response 
from utils.currency_utils import Change_columns_names , convert_matplotlib_image_to_base64,convert_dataFrame_to_plGraph
import matplotlib.pyplot as plt
from matplotlib import dates as mpl_dates

def currency_convertor(_from_currency:str,to_currency:str)->CurrencyConv_Response:
    _con=Exchange_connexion()
    Exchange:ForeignExchange=_con.Exchange
    print(_from_currency) 
    print(to_currency)
    Fixed_columns=["From_code","From_Currency_Name","To_code","To_Currency_Name","Exchange_Rate","Last_Refreshed","Time Zone","Bid_price","Ask_price"]

    [Convertion_result,meta]=Exchange.get_currency_exchange_rate(_from_currency,to_currency)
    Convertion_result=Change_columns_names(Convertion_result,Fixed_columns)
    print(Convertion_result)

    F_c= Convertion_result["From_Currency_Name"] [0]
    T_c= Convertion_result["To_Currency_Name"][0]
    Ex_r=float(Convertion_result["Exchange_Rate"][0]) 
    B_p =float(Convertion_result["Bid_price"][0]) 
    A_p =float(Convertion_result["Ask_price"][0] )
    L_r =Convertion_result["Last_Refreshed"][0] 
    
    return CurrencyConv_Response(From_Currency_Name=F_c,To_Currency_Name=T_c,Exchange_Rate=Ex_r,Bid_price=B_p,Ask_price=A_p,Last_Refreshed=L_r) 
    
    
Fixed_columns=["open","high","low","close"]
Fixed_colors=["green","orange","red","gray"]
Fixed_Data_type=[ "daily" ,"monthly" ,"weekly"  ,"interval" ]
def currency_statistic_image_daily(request)->CurrencyExchange_Image_response:

    _con=Exchange_connexion()
    Exchange:ForeignExchange=_con.Exchange
    [data,meta]=None
    try:
        print(request.Column_name)
        if(len(request.Column_name)>=1):
            print(data)

            if(request.Data_type==0):
                [data,meta]=Exchange.get_currency_exchange_daily(request.From_currency,request.To_currenct)
                print(data)
            elif(request.Data_type==1):    
                [data,meta]=Exchange.get_currency_exchange_weekly(request.From_currency,request.To_currenct)                
            elif(request.Data_type==2):    
                [data,meta]=Exchange.get_currency_exchange_monthly(request.From_currency,request.To_currenct)
            elif(request.Data_type==3):    
                [data,meta]=Exchange.get_currency_exchange_intraday(request.From_currency,request.To_currenct)
                
            data=Change_columns_names(data,Fixed_columns)   
                
                
            plt=convert_dataFrame_to_plGraph(data,request,plt)    
    except Exception as ex:
        print(ex)
    Image_data=convert_matplotlib_image_to_base64(plt,"png")
    return CurrencyExchange_Image_response(Image_base64=Image_data,image_detail={"Image_title":"non","Image_description":"none","Image_creationDate":"sdqsd"})

    
