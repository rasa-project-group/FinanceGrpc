import matplotlib
from ServerGrpc.entities.Requests import Requst_Currecny_Image
from typing import List
from pandas import DataFrame
import io 
import matplotlib.pyplot as plt
import base64
from PIL import Image
from matplotlib import dates as mpl_dates

def Change_columns_names(dataframe:DataFrame,new_columns:List)->DataFrame:
    """
    Change_columns_names Change the Dataframe columns names

    Args:
        dataframe (DataFrame): pandas dataframe
        new_columns (List): 

    Returns:
        DataFrame: Same Datafarme
    """
    
    if(len(dataframe.columns)==len(new_columns)):
            dataframe.columns=new_columns
            return dataframe
            
    return dataframe        
def convert_matplotlib_image_to_base64(plt:plt,format:str)->str:
    my_stringIObytes = io.BytesIO()
    plt.savefig(my_stringIObytes, format=format)
    my_stringIObytes.seek(0)
    my_base64_jpgData = base64.b64encode(my_stringIObytes.read())
    my_base_str=f"data:image/{format};base64,"+my_base64_jpgData.decode("utf-8")
    return my_base_str
def convert_dataFrame_to_plGraph(data:DataFrame,request:Requst_Currecny_Image,plt:plt)->plt:
        Fixed_columns=["open","high","low","close"]
        Fixed_colors=["green","orange","red","gray"]
        print(data)
        plt.figure(figsize=(15,8))
        plt.style.use("ggplot")
        for column_name in request.Column_name:
            if(column_name in Fixed_columns):
                plt.plot(data[column_name],label=column_name,c=Fixed_colors[Fixed_columns.index(column_name)])
        plt.xlabel('Date')
        plt.ylabel(f"{request.From_currency}/ {request.To_currenct}")
        plt.title(f"{request.From_currency} To {request.To_currenct} Convertor")
        date_format=mpl_dates.DateFormatter('%b, %d %Y')                                    
        plt.gca().xaxis.set_major_formatter(date_format)
        plt.gcf().autofmt_xdate()
        plt.legend()
        plt.legend(loc=1)
        plt.grid(True)
        return plt
            