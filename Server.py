from concurrent import futures
import grpc
import protos_output.finance_pb2_grpc
import protos_output.finance_pb2
import threading
from  ServerGrpc.Implementation.Imp_Currency_Exchange_Service import Imp_Curr_Exchange_service
import time
import os 

def server():
    os.environ["VANTAGE_KEY"]="MVDGLT2IXJSFSSSN"
    os.environ["OUTPUT_FORMAT"]="pandas"
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1))
    protos_output.finance_pb2_grpc.add_Currency_Exchange_ServiceServicer_to_server(Imp_Curr_Exchange_service(),server)
    server.add_insecure_port("[::]:9999")
    server.start()
    try:
        while True:
            print("server on: %i" % (threading.active_count()))
            time.sleep(10)
    except KeyboardInterrupt:
        print("finish")
        server.stop(0)


if __name__ == "__main__":
    server()